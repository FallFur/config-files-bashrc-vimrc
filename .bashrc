# Hi Hacker⠠⠵

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac
 
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/sbin:/usr/sbin:/usr/local/sbin:/snap/bin:~/Programs

HISTCONTROL=ignoreboth
 
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000
 
# check the window size after each command and, if necessary,
shopt -s checkwinsize
 
# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi
 
# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac
 
# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes
 
if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    color_prompt=yes
    else
    color_prompt=
    fi
fi
 
if [ "$color_prompt" = yes ]; then
    PS1="\[\033[0;31m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]☠\[\033[01;96m\]\h'; else echo '\[\033[0;39m\]\u\[\033[01;33m\]☠\[\033[01;96m\]\h'; fi)\[\033[0;31m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\$\\--> \[\e[0m\]"
else
    PS1='┌──[\u@\h]─[\w]\n└──╼ \$-->  '
fi
unset color_prompt force_color_prompt
 
# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\033[0;31m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]☠\[\033[01;96m\]\h'; else echo '\[\033[0;39m\]\u\[\033[01;33m\]☠\[\033[01;96m\]\h'; fi)\[\033[0;31m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\$\[\e[0m\]"
    ;;
*)
    ;;
esac

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
 
# enable color support
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
 
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

#Custom aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias mv='mv -iv'
alias cp='cp -iv'
alias rm='rm -v'
alias cdd='cd ${OLDPWD}'
alias untar='tar -xvf'
alias vi='vim'
alias fullminar='shred --size=10M --iterations=10 -u --verbose --zero --remove'
alias :q='exit'
alias cls='clear; ls -CF'
alias fuck='sudo $(history -p !!)'

#External aliases
alias navegador_de_la_ostia="firefox &"
alias youtube_m4a="youtube-dl --extract-audio --audio-format mp3"
alias fallfur_lgtb_touch="lolcat -a --speed=150 --freq=0.1"
alias connect="sudo openvpn"

#Variables
PUBLIC_IP=`wget http://ipecho.net/plain -O - -q ; echo`

#Traps
trap "echo Paso de tu Ctrl+c PRINGAO" 2

# tab complete even with sudo
#complete -cf sudo

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#RELOJ EN LA PARTE DE ARRIBA A LA DERECHA

#while sleep 1;do tput sc;tput cup 0 $(($(tput cols)-29));date;tput rc;done &

#AUTO_TMUX SIN ACOPLAR SESIONES
#ONLY ACTIVATE IF YOU HAVE INSTALLED TMUX

#1º Option
#tmux attach &> /dev/null

#2ª Option (better)
#if [[ ! $TERM =~ screen ]]; then
#	    exec tmux
#	    fi

#Auto mount shared folder
#mount -t vboxsf Documents ~/shared/

#echo "								                            ";
#echo "  █████▒▄▄▄       ██▓     ██▓      █████▒█    ██  ██▀███  ";
#echo "▓██   ▒▒████▄    ▓██▒    ▓██▒    ▓██   ▒ ██  ▓██▒▓██ ▒ ██▒";
#echo "▒████ ░▒██  ▀█▄  ▒██░    ▒██░    ▒████ ░▓██  ▒██░▓██ ░▄█ ▒";
#echo "░▓█▒  ░░██▄▄▄▄██ ▒██░    ▒██░    ░▓█▒  ░▓▓█  ░██░▒██▀▀█▄  ";
#echo "░▒█░    ▓█   ▓██▒░██████▒░██████▒░▒█░   ▒▒█████▓ ░██▓ ▒██▒";
#echo " ▒ ░    ▒▒   ▓▒█░░ ▒░▓  ░░ ▒░▓  ░ ▒ ░   ░▒▓▒ ▒ ▒ ░ ▒▓ ░▒▓░";
#echo " ░       ▒   ▒▒ ░░ ░ ▒  ░░ ░ ▒  ░ ░     ░░▒░ ░ ░   ░▒ ░ ▒░";
#echo " ░ ░     ░   ▒     ░ ░     ░ ░    ░ ░    ░░░ ░ ░   ░░   ░ ";
#echo "             ░  ░    ░  ░    ░  ░          ░        ░     ";
echo "							                            	";
echo " ~ Wellcome to your GNU/Linux system, Hacker ⠠⠵          ";
echo "							                                ";
echo " -!- Public IP: |$PUBLIC_IP| -!-                          ";
echo "                                                          ";
